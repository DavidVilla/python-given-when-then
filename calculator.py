#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

from traceback import print_exc
from unittest import TestCase


class GWT_clausule(object):
    OK = ' OK '
    FAIL = 'FAIL'
    ERROR = ' !! '

    def __init__(self, text):
        self.text = text

    def __enter__(self):
        pass

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type is None:
            return self.say(self.OK)

        self.catch(exc_val)

    def say(self, result, exc=None):
        sentence = self.__class__.__name__ + ':'
        msg = "[{0}] {1:6} {2}".format(result, sentence, self.text, result)
        if exc is not None:
            msg += "\n    " + self.format_exc(exc)

        print(msg)

    def format_exc(self, exc):
        return "{0}: {1}".format(exc.__class__.__name__, str(exc))

    def catch(self, exc):
        self.say(self.ERROR, exc)


class Given(GWT_clausule):
    def __enter__(self):
        print("--")
        GWT_clausule.__enter__(self)


class When(GWT_clausule):
    pass


class Then(GWT_clausule):
    def catch(self, exc):
        self.say(self.FAIL, exc)


class Calculator(object):
    def sum(self, n1, n2):
        return 4


class TestCalculator(TestCase):
    def test_ok(self):
        with Given("I have a calculator"):
            calculator = Calculator()

        with When("I enter with 2 + 2"):
            result = calculator.sum(2, 2)

        with Then("I have 4 as result"):
            self.assertEqual(result, 4)

    def test_error(self):
        with Given("I have a calculator"):
            calculator = Calculator()

        with When("I enter with 2 x 3"):
            result = calculator.multiply(2, 3)

        with Then("I have 6 as result"):
            self.assertEqual(result, 6)

    def test_fail(self):
        with Given("I have a calculator"):
            calculator = Calculator()

        with When("I enter with 1 + 1"):
            result = calculator.sum(1, 1)

        with Then("I have 2 as result"):
            self.assertEqual(result, 2)


# run with:
#
#    $ nosetests calculator.py -sq
#
